# Frontend Engineer Assessment - Reddit

For this challenge you're going to create a basic Reddit client app, this will give us an idea about your coding skills.

Your app should list the last posts in the r/programming subreddit to access this list use the following URL:

[https://api.reddit.com/r/programming/new](https://api.reddit.com/r/programming/new)

For more information about the JSON structure see: [https://github.com/reddit/reddit/wiki/JSON](https://github.com/reddit/reddit/wiki/JSON)

Other than the listing of the page, further designing of the features is up to you, creativity is appreciated. Some typical features:

- toggling of listing modes
- traversing through the list (pagination / infinite scrolling etc)
- sorting and filtering
- detail page of specific `reddit` and their comments
- query params integration

Tech stacks:

- React / React Native (if you are applying for Mobile role please use RN)
- Use Vite for React, and Expo for RN as template
- Use TypeScript (preferable)
- Optional inclusions
    - State mananegement (React Context, Redux etc)
    - Unit test (preferable)
    - Styling libs (SASS, Styled Components, CSS Modules, Tailwind etc)
    - UI Libraries (Material, AntD, Chakra etc)
    - error handling

Along with the submission would be great if you can provide your profiles of the following if applicable for our reference. It will help us tremendously to get to know you better.

- GitHub
- Code snippets/repo eg. codesandbox, codepen, stackblitz etc
- Coding challenges/certification sites eg. HackerRank, HackerEarth etc

## Submission requirements

- Please clearly state extra build and run requirements if any
- Please share the assignment via a PRIVATE git repository
- We want to observe your commit messages and the process, please commit module by module, instead of committing at one shot
- Please add users as stated in the email as collaborators
